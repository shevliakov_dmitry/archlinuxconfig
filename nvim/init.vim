set nocompatible
filetype off

call plug#begin()

Plug 'https://github.com/scrooloose/nerdtree.git'
Plug 'https://github.com/ntpeters/vim-better-whitespace.git'
Plug 'https://github.com/tpope/vim-surround.git'

Plug 'https://github.com/vim-airline/vim-airline.git'
Plug 'https://github.com/airblade/vim-gitgutter.git'
Plug 'https://github.com/tpope/vim-fugitive.git'

Plug 'https://github.com/easymotion/vim-easymotion.git'

"Syntax processing
Plug 'https://github.com/sheerun/vim-polyglot.git'
Plug 'https://github.com/autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }
Plug 'https://github.com/Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }


"Thermes
Plug 'https://github.com/hewo/vim-colorscheme-deepsea.git'
Plug 'https://github.com/rakr/vim-one.git'

call plug#end()

set shellslash

set hidden
set exrc
filetype plugin indent on
syntax on

set number
set expandtab
set tabstop=4
set shiftwidth=4

"Therme
set background=dark
set t_Co =255
colorscheme one

"Highlight the column
set colorcolumn=80

"VimAirline config
set laststatus=2
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tabline#fnamemod=':t'

"Vim Extra WhiteSpaces
highlight ExtraWhitespace ctermbg=LightRed guibg=LightRed

"VimGutter
set signcolumn=yes

"Polyglot
let g:cpp_class_scope_highlight = 1
let g:cpp_experimental_template_highlight = 1

"Vim-lsp
let g:LanguageClient_serverCommands = {
\ 'cpp': ['/usr/bin/cquery',
\ '--log-file=/tmp/cq.log',
\ '--init={"cacheDirectory":"/tmp/cquery/"}']
\ }

let g:LanguageClient_autoStart = 1
let g:deoplete#enable_at_startup = 1
nnoremap <F5> :call LanguageClient_contextMenu()<CR>

nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> gt :call LanguageClient#textDocument_typeDefinition()<CR>
nnoremap <F4>        :call LanguageClient#textDocument_references()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>
nnoremap <silent> :call LanguageClient#textDocument_hover()<CR>

"EasyMotion hotkeys
map <A-l> <Plug>(easymotion-overwin-line)
map <A-j> <Plug>(easymotion-j)
map <A-k> <Plug>(easymotion-k)
map <A-w> <Plug>(easymotion-w)
map <A-e> <Plug>(easymotion-e)
map <A-b> <Plug>(easymotion-b)
map <A-f> <Plug>(easymotion-f)
map <A-F> <Plug>(easymotion-F)
"NERDTree hotkeys
nmap <C-n> :NERDTreeToggle<CR>
"
"Buffer hotkeys
nmap <C-h> :bp<CR>
nmap <C-l> :bn<CR>
nmap <C-u> :bd<CR>
